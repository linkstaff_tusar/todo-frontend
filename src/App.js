import React, {component} from 'react';
import {BrowserRouter as Router, Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
import CreateTodo from "./components/create-todo.component";
import EditTodo from "./components/edit-todo.components";
import TodosList from "./components/todos-list.component";
import logo from "./logo.png"

let pageDesign = {
  padding: 0
}

function App() {
  return (
    <Router>
      <div className="container-fluid" style={pageDesign}>
      <nav className="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
            <a className="navbar-brand" href="https://google.com" target="_blank" rel="noopener noreferrer">
              <img src={logo} width="30" alt="logo" />
            </a>
            <Link to="/" className="navbar-brand">MERN-Stack App</Link>
            {/* <div className="collapse nav-collapse"> */}
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link to="/" className="nav-link">List</Link>
                </li>
                <li className="nav-item">
                  <Link to="/create" className="nav-link">Create</Link>
                </li>
                <li className="nav-item">
                  <Link to="/edit/:id" className="nav-link">Edit</Link>
                </li>
              </ul>
            {/* </div> */}
      </nav>
        <Route path="/" exact component={TodosList} />
        <Route path="/edit/:id" component={EditTodo} />
        <Route path="/create" component={CreateTodo} />
        </div>
    </Router>
  );
}

export default App;
