import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';

const Todo = props => (
    <tr>
        <td>{props.todo.todo_desc}</td>
        <td>{props.todo.todo_resp}</td>
        <td><Link to={"/edit/"+props.todo._id}>Edit</Link></td>
    </tr>
)
const alignment = {textAlign: "center"};
const border = {border : "1px solid black"};

export default class TodosList extends Component{
    constructor(props) {
        super(props);
        this.state = {todos : []};
    }
    componentDidMount(){
        axios.get('http://localhost:4000/todos/')
        .then(response=>{
            this.setState({todos:response.data});
        })
        .catch(function (error){
            console.log(error);
        })
    }
    Todosfunc(){
        return this.state.todos.map(function(currentTodo, i){
            return <Todo todo={currentTodo} key={i} />
        });
    }
    render(){
        return(
            <div className="container">
                <h3 style={alignment}>TodoList</h3>
                <table style={border} className="table table-dark table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Responsibility</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.Todosfunc()}
                    </tbody>
                </table>
            </div>
        )
    }
}