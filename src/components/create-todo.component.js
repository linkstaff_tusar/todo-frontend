import React, {Component} from 'react';
import axios from 'axios';

export default class CreateTodo extends Component{

    constructor(props){
        super(props);

        this.onChangeTodoDesc = this.onChangeTodoDesc.bind(this)
        this.onChangeTodoResp = this.onChangeTodoResp.bind(this)
        this.onSubmit = this.onSubmit.bind(this)

        this.state = {
            todo_desc: '',
            todo_resp: '',
            todo_completed: false
        }
    }

    onChangeTodoDesc(e){
        this.setState({
            todo_desc: e.target.value
        })
    }
    onChangeTodoResp(e){
        this.setState({
            todo_resp: e.target.value
        })
    }
    onSubmit(e){
        e.preventDefault();
        console.log(`Form Submitted with = ${this.state.todo_desc} ${this.state.todo_resp} ${this.state.todo_completed}`);
        const newTodo = {
            todo_desc: this.state.todo_desc,
            todo_resp: this.state.todo_resp,
            todo_completed: this.state.todo_completed
        }
        axios.post('http://localhost:4000/todos/add',newTodo)
        .then(res => console.log(res.data));
    }

    render() {
        return(
            <div style={{margin:300}}>
                <h3>Create New Todo</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Description:</label>
                        <input  type="text" 
                                className="form-control" 
                                value={this.state.todo_desc} 
                                onChange={this.onChangeTodoDesc}
                                />
                    </div>
                    <div className="form-group">
                        <label>Responsible:</label>
                        <input type="text" 
                                className="form-control" 
                                value={this.state.todo_resp} 
                                onChange={this.onChangeTodoResp}
                                />
                    </div>
                    <div className="form-group">
                            <input className="btn btn-primary" type="submit" value="Add Todo"/>
                    </div>
                </form>
            </div>
        )
    }
}

